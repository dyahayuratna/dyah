let map;

function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -6.917464, lng: 107.619125 },
    zoom: 8,
  });
}

window.initMap = initMap;